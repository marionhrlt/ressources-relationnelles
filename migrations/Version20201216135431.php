<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201216135431 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_ressource (category_id INT NOT NULL, ressource_id INT NOT NULL, INDEX IDX_54FF977412469DE2 (category_id), INDEX IDX_54FF9774FC6CD52A (ressource_id), PRIMARY KEY(category_id, ressource_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, ressource_id INT DEFAULT NULL, users_id INT DEFAULT NULL, content LONGTEXT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_9474526CFC6CD52A (ressource_id), INDEX IDX_9474526C67B3B43D (users_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE favorite (id INT AUTO_INCREMENT NOT NULL, ressources_id INT DEFAULT NULL, users_id INT DEFAULT NULL, INDEX IDX_68C58ED93C361826 (ressources_id), INDEX IDX_68C58ED967B3B43D (users_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE relation_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ressource (id INT AUTO_INCREMENT NOT NULL, creator_id INT NOT NULL, relation_type_id INT NOT NULL, ressource_type_id INT NOT NULL, status_id INT NOT NULL, title VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, summary VARCHAR(500) NOT NULL, created_at DATETIME NOT NULL, illustration VARCHAR(255) NOT NULL, INDEX IDX_939F454461220EA6 (creator_id), INDEX IDX_939F4544DC379EE2 (relation_type_id), INDEX IDX_939F454470760271 (ressource_type_id), INDEX IDX_939F45446BF700BD (status_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ressource_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE status (id INT AUTO_INCREMENT NOT NULL, favorite_id INT NOT NULL, entitled VARCHAR(255) NOT NULL, INDEX IDX_7B00651CAA17481D (favorite_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, status_id INT DEFAULT NULL, name VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(50) NOT NULL, email VARCHAR(100) NOT NULL, profil_picture VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D6495E237E06 (name), INDEX IDX_8D93D6496BF700BD (status_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category_ressource ADD CONSTRAINT FK_54FF977412469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_ressource ADD CONSTRAINT FK_54FF9774FC6CD52A FOREIGN KEY (ressource_id) REFERENCES ressource (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CFC6CD52A FOREIGN KEY (ressource_id) REFERENCES ressource (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C67B3B43D FOREIGN KEY (users_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE favorite ADD CONSTRAINT FK_68C58ED93C361826 FOREIGN KEY (ressources_id) REFERENCES ressource (id)');
        $this->addSql('ALTER TABLE favorite ADD CONSTRAINT FK_68C58ED967B3B43D FOREIGN KEY (users_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE ressource ADD CONSTRAINT FK_939F454461220EA6 FOREIGN KEY (creator_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE ressource ADD CONSTRAINT FK_939F4544DC379EE2 FOREIGN KEY (relation_type_id) REFERENCES relation_type (id)');
        $this->addSql('ALTER TABLE ressource ADD CONSTRAINT FK_939F454470760271 FOREIGN KEY (ressource_type_id) REFERENCES ressource_type (id)');
        $this->addSql('ALTER TABLE ressource ADD CONSTRAINT FK_939F45446BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
        $this->addSql('ALTER TABLE status ADD CONSTRAINT FK_7B00651CAA17481D FOREIGN KEY (favorite_id) REFERENCES favorite (id)');
        $this->addSql('ALTER TABLE `user` ADD CONSTRAINT FK_8D93D6496BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE category_ressource DROP FOREIGN KEY FK_54FF977412469DE2');
        $this->addSql('ALTER TABLE status DROP FOREIGN KEY FK_7B00651CAA17481D');
        $this->addSql('ALTER TABLE ressource DROP FOREIGN KEY FK_939F4544DC379EE2');
        $this->addSql('ALTER TABLE category_ressource DROP FOREIGN KEY FK_54FF9774FC6CD52A');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CFC6CD52A');
        $this->addSql('ALTER TABLE favorite DROP FOREIGN KEY FK_68C58ED93C361826');
        $this->addSql('ALTER TABLE ressource DROP FOREIGN KEY FK_939F454470760271');
        $this->addSql('ALTER TABLE ressource DROP FOREIGN KEY FK_939F45446BF700BD');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D6496BF700BD');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C67B3B43D');
        $this->addSql('ALTER TABLE favorite DROP FOREIGN KEY FK_68C58ED967B3B43D');
        $this->addSql('ALTER TABLE ressource DROP FOREIGN KEY FK_939F454461220EA6');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE category_ressource');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE favorite');
        $this->addSql('DROP TABLE relation_type');
        $this->addSql('DROP TABLE ressource');
        $this->addSql('DROP TABLE ressource_type');
        $this->addSql('DROP TABLE status');
        $this->addSql('DROP TABLE `user`');
    }
}
